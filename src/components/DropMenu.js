import { useMemo, useState } from "react";
import { DropDownList } from "@progress/kendo-react-dropdowns";
import { Item } from "semantic-ui-react";

const options = ["1", "2", "3", "4", "5", "6"];

const scoreOptions = [
  {
    id: 1,
    label: "1",
    type: "1",
  },
  {
    id: 2,
    label: "2",
    type: "2",
  },
  {
    id: 3,
    label: "3",
    type: "3",
  },
  {
    id: 4,
    label: "4",
    type: "4",
  },
  {
    id: 5,
    label: "5",
    type: "5",
  },
  {
    id: 6,
    label: "6",
    type: "6",
  },
];

export const DropdownScoreSelection = () => {
  const [options, setOptions] = useState("");

  const filteredData = useMemo(() => {
    //if (!options || options === "1") return scoreOptions;

    return scoreOptions.filter((item) => item.type === options);
  }, [options]);

  return (
    <section className="k-my-8">
      <form className="k-form k-mb-4">
        <label className="k-label k-mb-3">Please Give A Score</label>
        <DropDownList
          scoreOptions={options}
          defaultItem="Select a score"
          onChange={setOptions}
          //onChange={(e) => setOptions(e.value)}
        />
      </form>

      <section className="k-listgroup">
        <ul>
          {filteredData.map((item) => {
            return (
              <li key={item.id} className="k-listgroup-item">
                {item.label}
              </li>
            );
          })}
        </ul>
      </section>
    </section>
  );
};
