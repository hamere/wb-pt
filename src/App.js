import React, { Component } from "react";
import { Box } from "@material-ui/core";
import "./App.css";
import "@progress/kendo-theme-default/dist/all.css";
import { DropdownScoreSelection } from "./components/DropMenu";

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      names: "",
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit = (name) => {
    alert(`Submitted name: ${name}`);
  };

  handleChange = (e) => {
    this.setState({
      names: e.target.value,
    });
  };

  render() {
    return (
      <section className="flex">
        <div className="content flex">
          <div className="sandbox flex" align="center" padding="100px">
            <div className="directions">
              <h1>DISINFORMATION INSPECTION</h1>
            </div>

            <form id="nameForm" className="names flex">
              <div className="form-group" align="center">
                <textarea
                  id="names"
                  name="hard"
                  color="white"
                  bgcolor="white"
                  placeholder="Please Input the text here "
                  value={this.state.names}
                  cols={150}
                  rows={20}
                  onChange={this.handleChange}
                  wrap="hard"
                />
              </div>
              <label>Text Author : </label>

              <input type="textarea" name="textValue" />
            </form>

            <button id="TextButton" form="nameForm" type="submit" color="white">
              Submit
            </button>

            <div className="nametags flex">
              <div className="nametags-group flex">
                {/* {this.state.names
                  .split("\n")
                  .filter((n) => n) // to filter out empty names
                  .map((name, index) => (
                    <button
                      key={index}
                      type="button"
                      className="nametag"
                      onClick={() => this.handleSubmit(name)}
                    >
                      {name}
                    </button>
                  ))} */}
              </div>
              <div
                style={{ marginLeft: "400", marginTop: "60px", width: "50%" }}
              >
                <Box id="ExperBox" color="black" bgcolor="#ADD8E6" p={20}>
                  <h1>Score A-F</h1>
                  <h2>Expertise of the Author!</h2>
                  <h3>Category of Text</h3>
                </Box>
              </div>
              <div
                style={{ marginLeft: "400", marginTop: "60px", width: "50%" }}
              >
                <Box
                  id="vagoBox"
                  color="black"
                  bgcolor="#ADD8E6"
                  p={20}
                  border-radius="50"
                >
                  <h1>VAGO Score</h1>
                  <h3>Score Description</h3>
                </Box>
              </div>
              <div
                style={{ marginLeft: "400", marginTop: "60px", width: "50%" }}
              >
                <DropdownScoreSelection />
              </div>

              <div
                style={{ marginLeft: "400", marginTop: "60px", width: "50%" }}
              >
                <Box id="FinalBox" color="black" bgcolor="#ADD8E6" p={20}>
                  <h1>Final Assesment!</h1>
                  <h2>Disinformaiton Risk</h2>
                  <h3>Description</h3>
                </Box>
              </div>
              <button
                id="NewTextButton"
                form="nameForm"
                type="submit"
                paddingLeft="40"
                paddingRight="40"
                marginTop="10"
              >
                Insert New Text
              </button>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Content;
